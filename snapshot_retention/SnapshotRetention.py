#!/usr/bin/env python
"""
Snapshot Retention Class
"""
import dateutil.parser
import pytz
from datetime import datetime


class SnapshotRetention:
    """Class for finding the snapshots to retain, so the others might be
    removed.
    """

    def __init__(self, snapshots, policy=None):
        """

        @params snapshots A list of snapshots to process. Usually the
                          results from
                          boto.ec2.connect_to_region.get_all_snapshots()
                          call.

        @params policy Json describing a retention policy. There is a
                       default policy provided should the user exclude
                       it.

        """
        self.snapshots = sorted(snapshots, key=lambda k: k.start_time)
        if not policy:
            policy = {'interval': 15,
                      'hours': 2, 'days': 3, 'weeks': 3, 'months': 3}

        intervals = {'hours': 1, 'days': 1, 'weeks': 1, 'months': 1}
        """
        Determine intervals of backups. First we check if we are backing up
        less frequently than every hour. If so, interval['minutes']
        will be 0 and interval['hours'] will be 1440 (number of
        minutes in a day) / policy['interval'], unless, of course, the
        interval is longer than a day, then interval['days'] will also
        be zero, and so on, until we get a positive number for days,
        weeks, or months.

        """
        # If interval of snapshots is greater than 1 hour...
        if 60 / policy['interval'] < 1:
            # If interval of snapshots is greater than 1 day...
            if 1440 / policy['interval'] < 1:
                if 10080 / policy['interval'] < 1:
                    # If interval is
                    if 43200 / policy['interval'] < 1:
                        raise ValueError('Interval is over a month!')
                    else:
                        intervals['months'] = 43200 / policy['interval']
                else:
                    intervals['weeks'] = 10080 / policy['interval']
            else:
                intervals['days'] = 1440 / policy['interval']
        else:
            intervals['hours'] = 60 / policy['interval']

        # Interval hours, days, weeks, months
        self.i_hours = intervals['hours'] * policy['hours']
        self.i_days = intervals['days'] * policy['days']
        self.i_weeks = intervals['weeks'] * policy['weeks']
        self.i_months = intervals['months'] * policy['months']

        self.hours = policy['hours']
        self.days = policy['days']
        self.weeks = policy['weeks']
        self.months = policy['months']

    def _latest_snaphots(self, snaplist, n):
        """Returns the most recent list of the latest n members of a list of
        snapshots.

        """
        # Sort by date, most recent comes first.
        l = sorted(snaplist, key=lambda k: k.start_time, reverse=True)
        return l[:n]

    def get_snapshots_by_policy(self):
        """
        Returns a dictionary list of the snapshots to be retained based
        off the given policy.
        """
        now = pytz.utc.localize(datetime.utcnow())

        """ The return data, after applying the retention policy. """
        t_hours = []
        t_days = []
        t_weeks = []
        t_months = []

        """The return data, before applying the retention policy, keyed by
        the day delta for a_day, the week delta for a_week, and the
        month delta for a_month.
        """
        a_day = {}
        a_week = {}
        a_month = {}

        for x in self.snapshots:
            snapdate = dateutil.parser.parse(x.start_time)
            delta = now - snapdate
            hours = delta.total_seconds() / 60 / 60
            days = hours / 24
            weeks = days / 7
            months = days / 30
            if months <= self.months:
                if weeks <= self.weeks:
                    if days <= self.days:
                        if hours > self.hours:
                            for d in range(self.i_days):
                                if not str(d) in a_day:
                                    a_day.update({str(d): []})
                                if int(days) == d:
                                    a_day[str(d)].append(x)
                    else:
                        for w in range(self.i_weeks):
                            if not str(w) in a_week:
                                a_week.update({str(w): []})
                            if int(weeks) == w:
                                a_week[str(w)].append(x)
                else:
                    for m in range(self.i_months):
                        if not str(m) in a_month:
                            a_month.update({str(m): []})
                        if int(months) == m:
                            a_month[str(m)].append(x)
        t_hours = self._latest_snaphots(self.snapshots, self.i_hours)
        for day in range(self.i_days):
            t_days.extend(self._latest_snaphots(a_day[str(day)], 1))
        for week in range(self.i_weeks):
            t_weeks.extend(self._latest_snaphots(a_week[str(week)], 1))
        for month in range(self.i_months):
            t_months.extend(self._latest_snaphots(a_month[str(month)], 1))
        return ({'months': t_months,
                 'weeks': t_weeks,
                 'days': t_days,
                 'hours': t_hours})

    def test_results(self):
        """Pretty prints the results of get_snapshots_by_policy() for testing.
        """
        retain = self.get_snapshots_by_policy()
        for key in retain.keys():
            print '[{}]'.format(key)
            for x in retain[key]:
                print '\t{}\t{}'.format(x.id, x.start_time)
